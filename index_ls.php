<?php
// Допустим, все таблички созданы в БД "test».
// Bсе в utf8.

/*
 * "Набросал" и собрал методы-примитивы мускула
 * 

 *
 * yiimar, 02/2016
 */
class MySqlDr
{
    protected $_link;
    protected $_result;
    
    /**
     * Coздание соединения
     * 
     * @param type $mysql_host
     * @param type $mysql_user
     * @param type $mysql_password
     * @param type $database_name
     * @return type 
     */
    public function __construct($mysql_host, $mysql_user, $mysql_password, $database_name)
    {
        $this->getConnect($mysql_host, $mysql_user, $mysql_password);
        $this->setCharUTF8();
        $this->selectDB($database_name);
    }

    /**
     * Соединяемся, выбираем базу данных
     * 
     * @param type $host
     * @param type $user
     * @param type $password
     */
    protected function getConnect($mysql_host, $mysql_user, $mysql_password)
    {
        $this->_link = mysql_connect($mysql_host, $mysql_user, $mysql_password);

            if($this->_link)    echo 'Соединение успешно установлено'."<br>";
            else                die('Не удалось соединиться: ' . mysql_error());
    }
    
    /**
     * "убeраим кракозябры" при выводе
     * 
     * @param type $link
     * @return type boolean
     */
    protected function setCharUTF8()
    {
        return mysql_query(
                
              "SET character_set_results    = 'utf8',
                   character_set_client     = 'utf8',
                   character_set_connection = 'utf8',
                   character_set_database   = 'utf8',
                   character_set_server     = 'utf8'
              ",
                
              $this->_link
        );
    }    

    protected function selectDB($database_name)
    {
        if (!mysql_select_db($database_name, $this->_link)) {
            die('Не удалось выбрать базу данных');
        } else {
            echo "База успешно выбрана"."<br>";
            return true;
        }
    }    
    
    /**
     * Выполняем SQL-запрос
     * 
     * @param type $query string
     */
    public function queryRun($query)
    {
        $result = mysql_query($query, $this->_link) or die('Запрос не удался: ' . mysql_error());
        return $result;
    }

    public function resultToArray($result)
    {
        $resultArray = [];
        while(($resultArray[] = mysql_fetch_assoc($result)) || array_pop($resultArray));
        
        return $resultArray;
    }

    /**
     * Освобождаем память от результата
     * 
     * @param type $result
     * @return type boolean
     */
    public function clearMem($result)
    {
        return mysql_free_result($result);
    }

    /**
     * Закрываем соединение
     * 
     * @param type $link
     * @return type boolean
     */
    public function closeConnect()
    {
        mysql_close($this->_link);
        unset($this->_link);
    }
}
////////////////////////////////////

// Конфигурация
$mysql_host     = 'localhost';
$mysql_user     = 'root';
$mysql_password = 'mysql';
$database_name  = 'test';

////////////////////////////////////


$query1 = " 
SELECT 
               *
FROM
               category
WHERE
               parent_category_id IS NULL
               AND name LIKE 'авто%'
;
";
//SELECT t1.* FROM category t1 WHERE t1.id IN
$query2 = "
SELECT
    t1.id,
    COUNT(t2.id)
FROM
    category t1
    LEFT JOIN
        category t2
    ON
        t1.id = t2.parent_category_id
GROUP BY
    t1.id
HAVING
    COUNT(t2.id) <=3;
";
$query3 = "
SELECT 
                t1.*
FROM
                category t1
                                     LEFT JOIN
                                                         category t2
                                     ON 
                                                         t1.id = t2.parent_category_id
                                     WHERE 
                                                         t2.id IS NULL;
";
// Выполнение
$conn = new MySqlDr($mysql_host, $mysql_user, $mysql_password, $database_name);
$result  = $conn->queryRun($query1);
echo '<br>';
print_r($result);
$arr = $conn->resultToArray($result);

echo "<br>";
print_r($arr);
        // Выводим результаты в html
        echo "<table>\n";
       
            // строка заголовков
            echo "\t<tr>\n";
                for ($i=0; $i<mysql_num_fields($result); $i++) {
                    echo "\t\t<td>" . mysql_field_name($result, $i) . "</td>";
                }
            echo "\t</tr>\n";
            
            // строки таблицы выборки
            
            foreach ($arr as $row) {
                echo "\t<tr>\n";
                    foreach ($row as $value) {
                        echo "\t\t<td>$value</td>";
                    }                    
                echo "\t</tr>\n";
            }
            
        echo "</table>\n";
        
$conn->clearMem($result);
$conn->closeConnect();

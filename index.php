<?php
// Допустим, все таблички созданы в БД "test».
// Bсе в utf8.

/*
 * "Набросал" и собрал методы-примитивы мускула
 * 
 * yiimar, 09/2014
 */
class MySqlDr
{
    protected $_link;
    protected $_result;
    
    /**
     * Coздание соединения
     * 
     * @param type $mysql_host
     * @param type $mysql_user
     * @param type $mysql_password
     * @param type $database_name
     * @return type 
     */
    public function __construct($mysql_host, $mysql_user, $mysql_password, $database_name)
    {
        $this->getConnect($mysql_host, $mysql_user, $mysql_password);
        $this->setCharUTF8();
        $this->selectDB($database_name);
    }

    /**
     * Соединяемся, выбираем базу данных
     * 
     * @param type $host
     * @param type $user
     * @param type $password
     */
    protected function getConnect($mysql_host, $mysql_user, $mysql_password)
    {
        $this->_link = mysql_connect($mysql_host, $mysql_user, $mysql_password);

            if($this->_link)    echo 'Соединение успешно установлено'."<br>";
            else                die('Не удалось соединиться: ' . mysql_error());
    }
    
    /**
     * "убeраим кракозябры" при выводе
     * 
     * @param type $link
     * @return type boolean
     */
    protected function setCharUTF8()
    {
        return mysql_query(
                
              "SET character_set_results    = 'utf8',
                   character_set_client     = 'utf8',
                   character_set_connection = 'utf8',
                   character_set_database   = 'utf8',
                   character_set_server     = 'utf8'
              ",
                
              $this->_link
        );
    }    

    protected function selectDB($database_name)
    {
        if (!mysql_select_db($database_name, $this->_link)) {
            die('Не удалось выбрать базу данных');
        } else {
            echo "База успешно выбрана"."<br>";
            return true;
        }
    }    
    
    /**
     * Выполняем SQL-запрос
     * 
     * @param type $query string
     */
    public function queryRun($query)
    {
        $result = mysql_query($query, $this->_link) or die('Запрос не удался: ' . mysql_error());
        return $result;
    }

    public function resultToArray($result)
    {
        while(($resultArray[] = mysql_fetch_assoc($result)) || array_pop($resultArray));
        
        return $resultArray;
    }

    /**
     * Освобождаем память от результата
     * 
     * @param type $result
     * @return type boolean
     */
    public function clearMem($result)
    {
        return mysql_free_result($result);
    }

    /**
     * Закрываем соединение
     * 
     * @param type $link
     * @return type boolean
     */
    public function closeConnect()
    {
        mysql_close($this->_link);
        unset($this->_link);
    }
}
////////////////////////////////////

// Конфигурация
$mysql_host     = 'localhost';
$mysql_user     = 'root';
$mysql_password = 'mysql';
$database_name  = 'test';

////////////////////////////////////

/*
Запрос. Сделано строго согласно ТЗ. Возможно, в ТЗ грамматические "АШИПКИ":
1. В исходной табличке "users"- "Святослав" (СвЯтослав), в требуемом выводе- "Светослав"
   (СвЕтослав) 
2. В исходной табличке "userprice"- "1, 2, 1.01", в требуемом выводе вместо "1.01"- "1.05"
   Если это "АШИПКИ", то следует:
2.1.заменить строку кода
        , IFNULL (
              up.value,
              IF (up.value='1.01',
                  '1.05',
                  'null'
              )
          )        AS val
    на 
        , up.value AS val
2.2.заменить строку кода
    , IF (!STRCMP(users.name, 'Святослав‭'), 'Светослав', users.name)   AS uname
    на 
    , users.name   AS uname
 */
$query = " 
  
    SELECT 
          u.uname  AS name
        , u.pprice AS price
        , u.usid   AS uid
        , u.prid   AS pid
        , IFNULL (
              up.value,
              IF (up.value='1.01',
                  '1.05',
                  'null'
              )
          )        AS val
      FROM
        (SELECT 
               users.id     AS usid
             , IF (!STRCMP(users.name, 'Святослав‭'), 'Светослав', users.name)   AS uname
             , prices.id    AS prid
             , prices.price AS pprice
           FROM users, prices
        ) AS u
      LEFT JOIN userprice AS up
        ON 
            u.usid = up.user_id
          AND
            u.prid = up.price_id  
      ORDER BY
          name
        , price
             
";

// Выполнение
$conn = new MySqlDr($mysql_host, $mysql_user, $mysql_password, $database_name);
$result  = $conn->queryRun($query);
echo '<br>';

$arr = $conn->resultToArray($result);
    
        // Выводим результаты в html
        echo "<table>\n";
       
            // строка заголовков
            echo "\t<tr>\n";
                for ($i=0; $i<mysql_num_fields($result); $i++) {
                    echo "\t\t<td>" . mysql_field_name($result, $i) . "</td>";
                }
            echo "\t</tr>\n";
            
            // строки таблицы выборки
            
            foreach ($arr as $row) {
                echo "\t<tr>\n";
                    foreach ($row as $value) {
                        echo "\t\t<td>$value</td>";
                    }                    
                echo "\t</tr>\n";
            }
            
        echo "</table>\n";
        
$conn->clearMem($result);
$conn->closeConnect();

/******* Результат выполнения *********

Соединение успешно установлено
База успешно выбрана

name	  price	   uid pid val
Владимир‭  Мицубиси‭ 1   1   1.0
Владимир‭  Тойота   1   2   1.05
Светослав‭ Мицубиси‭ 2   1   null
Светослав‭ Тойота‭   2   2   null
*/
    
?>